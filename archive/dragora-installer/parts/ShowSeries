# This file is part of the 'dragora-installer'.
#
# Purpose: Search to show the detected series of the found medium.

MEDIUM_SOURCE="$(cat -- ${SUBTMPDIR}/MediumFound)"
export MEDIUM_SOURCE

cat << EOF > ${SUBTMPDIR}/ShowSeries
dialog --colors \\
 --backtitle "\ZbChoosing the series" \\
 --title "SOFTWARE SERIES" \\
 --no-cancel --item-help --checklist \\
"The following series of software were found for installation.\n\n\\
Use the cursor keys and \\Z3[Space]\\Zn to deselect the software series\n\\
that you do not want to include.  Once ready, press \\Z3[Enter]\\Zn to \
continue..." $(( LINES - 6 )) 69 12 \\
EOF

find -- "$MEDIUM_SOURCE" -type f \( -name '*@*.tlz' -a ! -name '*-pass*' \) \
 -exec sh -c 'name="${1##*@}"; name="${name%.tlz}"; echo "$name"' _ {} \; | \
  sort -u | while read -r category
do
    case $category in
    boot*)
        echo '"boot" "" ON "Necessary software to boot the system." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    braille*)
        echo '"braille" "" ON "Braille embossing support." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    compressors*)
        echo '"compressors" "" ON "Several compressors at user-level." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    daemons*)
        echo '"daemons" "" ON "System-related daemons." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    data*)
        echo '"data" "" ON "Essential configuration files." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    db*)
        echo '"db" "" ON "Databases such as BerkeleyDB, GDBM, and the system Time Zone Database (tzdb)." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    devel*)
        echo '"devel" "" ON "Compilers, programming languages and debuggers." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    dict*)
        echo '"dict" "" ON "Spell checker software and dictionaries." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    docbook*)
        echo '"docbook" "" ON "SGML and XML support for DocBook documents." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    gnupg*)
        echo '"gnupg" "" ON "The GNU privacy guard." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    kernel*)
        echo '"kernel" "" ON "The Linux-libre kernel and related." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    libs*)
        echo '"libs" "" ON "The system libraries." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    lua*)
        echo '"lua" "" ON "The Lua programming language and modules." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    manuals*)
        echo '"manuals" "" ON "The manual pages." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    media*)
        echo '"media" "" ON "Multimedia related utilities and libraries." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    networking*)
        echo '"networking" "" ON "Clients and servers for networking." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    perl*)
        echo '"perl" "" ON "The Perl programming language and modules." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    python*)
        echo '"python" "" ON "The Python programming language and modules." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    ruby*)
        echo '"ruby" "" ON "The Ruby programming language and modules." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    scanning*)
        echo '"scanning" "" ON "Scanning support." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    shells*)
        echo '"shells" "" ON "Command interpreters for the system." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    tools*)
        echo '"tools" "" ON "Tools and complements of the base system." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    x-apps*)
        echo '"x-apps" "" ON "A useful collection of applications for the X Window System." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    x-libs*)
        echo '"x-libs" "" ON "A useful collection of libraries for the X Window System." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_app*)
        echo '"xorg_app" "" ON "The X Window System applications." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_data*)
        echo '"xorg_data" "" ON "Bitmaps, cursor themes and configuration files." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_doc*)
        echo '"xorg_doc" "" ON "The X Window System documentation." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_driver*)
        echo '"xorg_driver" "" ON "Video and Input drivers for the X Window System." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_extra*)
        echo '"xorg_extra" "" ON "Extra libraries and essential utilities for the X Window System." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_font*)
        echo '"xorg_font" "" ON "The X Window System font files." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_lib*)
        echo '"xorg_lib" "" ON "The X Window System libraries." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_mesa*)
        echo '"xorg_mesa" "" ON "The Mesa 3D graphics library." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_proto*)
        echo '"xorg_proto" "" ON "The X Window System header(s) for development." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_util*)
        echo '"xorg_util" "" ON "The X Window System utilities." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_xcb*)
        echo '"xorg_xcb" "" ON "The XCB protocol." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    xorg_xserver*)
        echo '"xorg_xserver" "" ON "The X Window System server." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    tde_i18n*)
        echo '"tde_i18n" "" ON "Internationalization translations for TDE." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    tde*)
        echo '"tde" "" ON "The Trinity Desktop Environment (TDE)." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    printing*)
        echo '"printing" "" ON "Printing support." \' >> ${SUBTMPDIR}/ShowSeries
        ;;
    *)
        echo "\"${category}\" \"\" ON \"Description not available.\" \\" >> ${SUBTMPDIR}/ShowSeries
        ;;
    esac
done

# Check if there are any detected series
if test ! -f ${SUBTMPDIR}/ShowSeries || test ! -s ${SUBTMPDIR}/ShowSeries
then
    echo "${PROGRAM}: Error no series found.  Aborting..." 1>&2
    exit 99;
fi
echo ' 2> ${SUBTMPDIR}/return-ShowSeries' >> ${SUBTMPDIR}/ShowSeries

touch ${SUBTMPDIR}/return-ShowSeries

. ${SUBTMPDIR}/ShowSeries;	# Show menu.
if test ! -f ${SUBTMPDIR}/return-ShowSeries || test ! -s ${SUBTMPDIR}/return-ShowSeries
then
    echo "${PROGRAM}: No series were selected.  Aborting..." 1>&2
    exit 99;
fi

# Append a new line for reading (this will be done from 'InstallPackages')
echo "" >> ${SUBTMPDIR}/return-ShowSeries

