## archive/ - Contains specific files to be included from recipes.

The sub-directories listed here are represented using the name of the
software to be packaged or using the package name.  Sub-directories
can contain configuration files, headers and substantial files to be
used from a specific recipe (which is where the final software package
will be incorporated and produced from).

