#! /bin/sh -
# Copyright (c) 2017-2018 Matias Fonzo.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if test $# -eq 0
then
    echo "An alternative to Debian run-parts(8)"
    echo ""
    echo "Usage: $0 <directory>"
    exit 0
fi
if test ! -d "$1"
then
    echo "Error '$1' is not a valid directory." 1>&2
    echo "Usage: $0 <directory>" 1>&2
    exit 1
fi

SUFFIXES_TO_IGNORE="~ ^ , .bak .new"

# Loop
for script in "$1"/*
do
    test -f "$script" || continue;

    for suffix in $SUFFIXES_TO_IGNORE
    do
        case $script in
        *${suffix})
            continue 2
            ;;
        esac
    done
    unset suffix

    if test -x "$script"
    then
        "$script" || echo "$script failed." 1>&2
    fi
done

