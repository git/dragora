# Build recipe for alsa-lib.
#
# Copyright (c) 2017-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=alsa-lib
version=1.2.8
release=1

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=https://www.alsa-project.org/files/pub/lib/$tarname

description="
The ALSA library.

The alsa-lib package contains the ALSA library.  This is used by
programs (including ALSA utilities) requiring access to the ALSA
sound interface.
"

homepage=https://alsa-project.org
license=LGPLv2.1

# Source documentation
docs="COPYING ChangeLog MEMORY-LEAK NOTES TODO version doc/asoundrc.txt"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --without-versioned \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

