# Build recipe for libtasn1.
#
# Copyright (c) 2018-2019, 2021-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=libtasn1
version=4.19.0
release=1

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://ftp.gnu.org/gnu/libtasn1/$tarname

description="
A highly portable C library that encodes or decodes an ASN.1 schema.

Libtasn1 is the ASN.1 library used by GnuTLS, p11-kit and some other
packages.  It was originally written by Fabio Fiorina, and now
maintained as a GNU package.  The goal of this implementation is to
be highly portable, and only require an ANSI C99 platform.
"

homepage=https://www.gnu.org/software/libtasn1
license="GPLv3+, LGPLv2+"

# Source documentation
docs="AUTHORS CONTRIBUTING.md COPYING ChangeLog NEWS README.md THANKS"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure \
    CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --enable-static=no \
     --enable-shared=yes \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

