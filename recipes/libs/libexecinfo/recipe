# Build recipe for libexecinfo.
#
# Copyright (c) 2022-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=libexecinfo
version=1.1
release=2

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=http://distcache.freebsd.org/local-distfiles/itetcu/$tarname

description="
Clone of the GNU libc backtrace facility.

This is a quick-n-dirty BSD licensed clone of backtrace facility found
in the GNU libc, mainly intended for porting Linuxish code to BSD
platforms, however it can be used at any platform which has a GCC
compiler.
"

homepage=http://www.freshports.org/devel/libexecinfo
license="BSD 2-clause"

# Source documentation
docs="README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Apply patches from "Void Linux" in order to build Rust
    patch -Np1 -i "${worktree}/patches/libexecinfo/01-execinfo.patch"
    patch -Np1 -i "${worktree}/patches/libexecinfo/02-makefile.patch"
    patch -Np1 -i "${worktree}/patches/libexecinfo/03-define-gnu-source.patch"
    patch -Np1 -i "${worktree}/patches/libexecinfo/libexecinfo_pc.patch"

    make -j${jobs} V=1 \
     CFLAGS="$QICFLAGS -fno-stack-protector" \
     LDFLAGS="$(echo "$QILDFLAGS" | sed 's/-s//')"

    sed -e "/Version:/s@version@${version}@" -i libexecinfo.pc

    mkdir -p "${destdir}/usr/include" "${destdir}/usr/lib${libSuffix}/pkgconfig"

    for header in *.h
    do
        install -p -m 644 "$header" "${destdir}/usr/include"
    done
    unset -v header

    for library in *.a *.so*
    do
        install -p -m 755 "$library" "${destdir}/usr/lib${libSuffix}"
    done
    unset -v library
    chmod 644 "${destdir}/usr/lib${libSuffix}"/*.a

    install -p -m 644 libexecinfo.pc "${destdir}/usr/lib${libSuffix}/pkgconfig"

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
    head -n 28 execinfo.c > "${destdir}/${docsdir}/LICENSE"
}

