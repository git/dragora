# Build recipe for isl.
#
# Copyright (c) 2016-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=isl
version=0.24
release=1

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=https://libisl.sourceforge.io/$tarname

description="
A library of integers.

ISL is a library for manipulating sets and relations of integer points
bounded by linear constraints.  Supported operations on sets include
intersection, union, set difference, emptiness check, convex hull,
(integer) affine hull, integer projection, and computing the
lexicographic minimum using parametric integer programming.

It also includes an ILP solver based on generalized basis reduction.
"

homepage=https://freshmeat.net/projects/isl
license=MIT

# Source documentation
docs="AUTHORS ChangeLog LICENSE README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Update for hosts based on musl
    cp -f "${worktree}/archive/common/config.guess" config.guess
    cp -f "${worktree}/archive/common/config.sub" config.sub

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --enable-static \
     --enable-shared \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Move misplaced file(s) for GDB
    mkdir -p "${destdir}/usr/share/gdb/auto-load/usr/lib${libSuffix}"
    mv "${destdir}/usr/lib${libSuffix}"/*-gdb.py \
       "${destdir}/usr/share/gdb/auto-load/usr/lib${libSuffix}"

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

