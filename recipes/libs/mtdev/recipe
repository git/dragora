# Build recipe for mtdev.
#
# Copyright (c) 2017 Mateus P. Rodrigues, <mprodrigues@dragora.org>.
# Copyright (c) 2021 Matias A. Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=mtdev
version=1.1.6
release=1

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=https://bitmath.org/code/mtdev/$tarname

description="
Multitouch Protocol Translation Library.

The mtdev is a stand-alone library which transforms all variants of
kernel MT events to the slotted type B protocol.  The events put into
mtdev may be from any MT device, specifically type A without contact
tracking, type A with contact tracking, or type B with contact
tracking.  See the kernel documentation for further details.
"

homepage=https://bitmath.org/code/mtdev/
license=MIT

# Source documentation
docs="COPYING ChangeLog README"
docsdir="${docdir}/${pkgname}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Update for several hosts (including Musl)
    cp -f "${worktree}/archive/common/config.guess" config-aux/config.guess
    cp -f "${worktree}/archive/common/config.sub" config-aux/config.sub

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --docdir=$docsdir \
     --infodir=$infodir \
     --mandir=$mandir \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

