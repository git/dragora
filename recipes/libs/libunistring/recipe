# Build recipe for libunistring.
#
# Copyright (c) 2018, 2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=libunistring
version=1.1
release=1

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://ftp.gnu.org/gnu/libunistring/$tarname

description="
A library that provides functions for manipulating Unicode strings.

Text files are nowadays usually encoded in Unicode, and may consist of
very different scripts from Latin letters to Chinese Hanzi, with many
kinds of special characters accents, right-to-left writing marks,
hyphens, Roman numbers, and much more.  But the POSIX platform APIs for
text do not contain adequate functions for dealing with particular
properties of many Unicode characters.  In fact, the POSIX APIs for
text have several assumptions at their base which don't hold for
Unicode text.

This library provides functions for manipulating Unicode strings and
for manipulating C strings according to the Unicode standard.
"

homepage=https://www.gnu.org/software/libunistring/
license="GPLv3+, LGPLv3+"

# Source documentation
docs="AUTHORS BUGS COPYING* HACKING NEWS README THANKS"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --enable-static=no \
     --enable-shared=yes \
     --enable-threads=posix \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Delete generated charset.alias
    rm -f "${destdir}/usr/lib${libSuffix}/charset.alias"
    rmdir "${destdir}/usr/lib${libSuffix}/" 2> /dev/null || true

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

