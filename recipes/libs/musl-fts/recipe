# Build recipe for musl-fts.
#
# Copyright (c) 2020-2021 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=musl-fts
version=1.2.7
release=1

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://github.com/void-linux/musl-fts/archive/v${version}/$tarname

description="
Implementation of fts(3) functions for musl libc.

The musl-fts package implements the fts(3) functions
fts_open, fts_read, fts_children, fts_set and fts_close,
which are missing in musl libc.

It uses the NetBSD implementation of fts(3) to build a static
library /usr/lib/libfts.a and the /usr/include/fts.h header file.
"

homepage=https://github.com/pullmoll/musl-fts
license="BSD 3-clause"

# Source documentation
docs="AUTHORS COPYING ChangeLog NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    sh ./bootstrap.sh

   ./configure CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    install -p -m 0644 fts.3 -D "${destdir}/${mandir}/man3/fts.3"
    lzip -9 "${destdir}/${mandir}/man3/fts.3"

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

