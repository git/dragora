# Build recipe for mpfr.
#
# Copyright (c) 2015-2020 Matias Fonzo, <selk@dragora.org>.
# Copyright (c) 2022-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=mpfr
version=4.2.0
release=1

# Define a category for the output of the package name
pkgcategory=libs

tarname=${program}-${version}.tar.bz2

# Remote source(s)
fetch=https://www.mpfr.org/mpfr-current/$tarname

description="
Library for multiple-precision floating point.

The MPFR library is a C library for multiple-precision
floating-point computations with correct rounding.
"

homepage=https://www.mpfr.org
license="GPLv3+, LGPLv3+"

# Source documentation
docs="AUTHORS BUGS COPYING* ChangeLog NEWS PATCHES README TODO VERSION"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Apply patches from upstream

    for file in "${worktree}"/patches/mpfr/*
    do
        if test -f "$file"
        then
            rm -f PATCHES
            patch -p1 < "$file"
        fi
    done

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" CXXFLAGS="$QICXXFLAGS" LDFLAGS="$QILDFLAGS" \
    MAKEINFO="true" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --docdir=$docsdir \
     --enable-static \
     --enable-shared \
     --enable-thread-safe \
     --build="$(gcc -dumpmachine)"

    make -j${jobs}
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/* || true
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

