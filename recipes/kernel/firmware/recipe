# Build recipe for linux-libre-firmware.
#
# Copyright (c) 2017-2018, 2021-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=linux-libre-firmware
version=1.4
release=2

# Define a category for the output of the package name
pkgcategory=kernel

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch=https://jxself.org/firmware/$tarname

description="
The GNU Linux-libre firmware.

This is the release of free firmware for use with the linux-libre kernel.

For more information, please read:
https://jxself.org/free-firmware.shtml
"

homepage=https://jxself.org/firmware
license=Custom

# Source documentation
docs="GPL-2 GPL-3 MPL-2 README WHENCE"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Prepare package to copy the firmwares (data)

    mkdir -p "${destdir}/lib/firmware"
    cp -R -p bin/* "${destdir}/lib/firmware/"
    chown -R root:root "${destdir}/lib/firmware/"

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cd src && cp -p $docs "${destdir}/$docsdir"
}

