# Build recipe for xinit.
#
# Copyright (c) 2017 Mateus P. Rodrigues <mprodrigues@dragora.org>.
# Copyright (c) 2017-2019, 2021-2022 Matias Fonzo <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=xinit
version=1.4.2
release=1

# Define a category for the output of the package name
pkgcategory=xorg_app

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://www.x.org/releases/individual/app/$tarname

description="
X Window System initializer.

The xinit program is used to start the X Window System server and a
first client program on systems that are not using a display manager
such as xdm(1) or in environments that use multiple window systems.
"

homepage=https://www.x.org
license="MIT X Consortium"

# Source documentation
docs="COPYING ChangeLog README.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # We don't support systemd.
    #
    # Apply a patch from "Slackware Linux".
    patch -Np1 -i "${worktree}/patches/xinit/xinit.remove.systemd.kludge.diff"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --docdir=$docsdir \
     --with-xinitdir=/etc/X11/xinit

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Hence the X session starts on the first unused virtual terminal,
    # which normally is vt7 (Thanks to BLFS!) ...
    sed -e '/$serverargs $vtarg/ s/serverargs/: #&/' \
        -i "${destdir}/usr/bin/startx"

    # Delete default xinitrc for TWM, this is
    # provided by the twm package now

    rm -f "${destdir}/etc/X11/xinit/xinitrc"

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

