# Build recipe for xfd.
#
# Copyright (c) 2017 Mateus P. Rodrigues <mprodrigues@dragora.org>.
# Copyright (c) 2017, 2019, 2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=xfd
version=1.1.4
release=1

# Define a category for the output of the package name
pkgcategory=xorg_app

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://www.x.org/releases/individual/app/$tarname

description="
Display all the characters in an X font.

xfd displays all the characters in a font using either the X11 core
protocol or libXft2.
"

homepage=https://www.x.org
license="MIT X Consortium"

# Source documentation
docs="COPYING ChangeLog README.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --with-appdefaultdir=/etc/X11/app-defaults \
     || true

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    touch "${destdir}/etc/X11/app-defaults/.graft-config"

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

