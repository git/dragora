# Build recipe for xorg-server.
#
# Copyright (c) 2017 Mateus P. Rodrigues <mprodrigues@dragora.org>.
# Copyright (c) 2017-2019, 2021-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=xorg-server
version=21.1.8
release=1

# Define a category for the output of the package name
pkgcategory=xorg_xserver

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://www.x.org/releases/individual/xserver/$tarname

description="
A portable, network-transparent Window System.

The X Window System is a network transparent window system which
runs on a wide range of computing and graphics machines.
"

homepage=https://www.x.org
license="MIT X Consortium"

# Source documentation
docs="COPYING ChangeLog README.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Adjust headers (thanks to our team mate "mprodrigues")
    sed -i 's#termio.h#termios.h#' hw/xfree86/os-support/xf86_OSlib.h

    rm -rf BUILD
    mkdir BUILD
    cd BUILD

    CPPFLAGS="$QICPPFLAGS -D_GNU_SOURCE -D__gid_t=gid_t -D__uid_t=uid_t" \
    CFLAGS="$QICFLAGS " LDFLAGS="$QILDFLAGS -Wl,-z,lazy" \
    meson setup $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --buildtype=release \
     --strip \
     -Dxorg=true \
     -Dxnest=true \
     -Dglamor=true \
     -Dbuilder_addr='dragora-users@nongnu.org' \
     -Dmodule_dir=/usr/lib${libSuffix}/xorg/modules \
     -Ddefault_font_path="/usr/share/fonts/X11/misc,/usr/share/fonts/X11/Type1,/usr/share/fonts/X11/100dpi:unscaled,/usr/share/fonts/X11/75dpi:unscaled,/usr/share/fonts/X11/cyrillic,/usr/share/fonts/X11/TTF,/usr/share/fonts/X11/OTF" \
     -Dglx=true \
     -Dsecure-rpc=false \
     -Dipv6=true \
     -Dxkb_dir=/usr/share/X11/xkb \
     -Dxkb_output_dir=/var/lib/xkb \
     -Dlisten_tcp=false \
     -Dlisten_unix=true \
     -Dlisten_local=true \
     -Dint10=x86emu \
     -Dsuid_wrapper=true \
     -Dhal=false \
     -Dsystemd_logind=false \
     -Dxf86bigfont=true \
     -Dxcsecurity=true \
     -Dmitshm=true \
     -Ddri1=true \
     -Ddri2=true \
     -Ddri3=true \
     ..

    ninja -j${jobs}
    DESTDIR="$destdir" ninja -j${jobs} install

    cd ..

    # Allow old behavior to terminate server keystroke
    cp -p "${worktree}/archive/xserver/00-keyboard.conf" \
          "${destdir}/usr/share/X11/xorg.conf.d"
    chmod 644 "${destdir}/usr/share/X11/xorg.conf.d/00-keyboard.conf"

    # Set the suid bit on the Xorg application.
    #
    # This will allow us to switch between tty1 and tty7 with the
    # Ctrl-Alt-F1 and Ctrl-Alt-F7 key combinations (see
    # recipes/xorg/app/xinit/recipe).  Thanks to BLFS!
    chmod 4755 "${destdir}/usr/libexec/Xorg"

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

