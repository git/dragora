# Build recipe for lua.
#
# Copyright (C) 2020 Kevin "The Nuclear" Bloom, <kevin.bloom@posteo.net>.
# Copyright (c) 2020-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=lua
version=5.4.4
short_version="${version%.*}"
release=1

# Set custom package name to avoid conflicts on package upgrades
pkgname="lua${short_version%.*}"

# Define a category for the output of the package name
pkgcategory=lua

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://www.lua.org/ftp/${tarname}

description="
A lightweight programming language designed for extending applications.

Lua is a powerful, efficient, lightweight, embeddable scripting language.
It supports procedural programming, object-oriented programming, functional
programming, data-driven programming, and data description.
"

homepage=https://www.lua.org/
license="MIT License"

# Source documentation
docs="doc/*.html doc/*.css doc/*.gif doc/*.png"
docsdir="${docdir}/${pkgname}-${version}"

_make_target()
{
    make -j${jobs} \
     MYCFLAGS="$QICFLAGS -DLUA_COMPAT_5_2 -DLUA_COMPAT_5_1 -fPIC" \
     MYLDFLAGS="$QILDFLAGS" \
     INSTALL_LIB="${destdir}/usr/lib${libSuffix}" \
     INSTALL_LMOD="${destdir}/usr/share/lua/${short_version}" \
     INSTALL_CMOD="${destdir}/usr/lib${libSuffix}/lua/${short_version}" \
     INSTALL_DATA="cp -d" \
     INSTALL_MAN="${destdir}/${mandir}/man1" \
     INSTALL_TOP="${destdir}/usr" "$@"
}

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    patch -Np1 -i "${worktree}/patches/lua/lua-5.4.2-shared_library-1.patch"

    # Adjust Lua search path
    sed -e 's#/usr/local/#/usr/#' \
        -e "s#lib/lua#lib${libSuffix}/lua#" \
        -i src/luaconf.h

    _make_target linux
    _make_target install

    # Now build the shared library

    make clean

    _make_target TO_LIB="liblua.so liblua.so.${short_version} liblua.so.${version}" linux
    _make_target TO_LIB="liblua.so liblua.so.${short_version} liblua.so.${version}" install

    unset -v make_target

    # Strip remaining binaries and libraries
    find "$destdir" -type f | xargs file | \
     awk '/ELF/ && /executable/ || /shared object/' | \
      cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

    # Install a pkg-config file
    mkdir -p "${destdir}/usr/lib${libSuffix}/pkgconfig"
    cat << EOF > "${destdir}/usr/lib${libSuffix}/pkgconfig/lua.pc"
prefix=/usr
INSTALL_BIN=\${prefix}/bin
INSTALL_INC=\${prefix}/include
INSTALL_LIB=\${prefix}/lib
INSTALL_MAN=\${prefix}/share/man/man1
INSTALL_LMOD=\${prefix}/share/lua/${short_version}
INSTALL_CMOD=\${prefix}/lib${libSuffix}/lua/${short_version}
exec_prefix=\${prefix}
libdir=\${exec_prefix}/lib${libSuffix}
includedir=\${prefix}/include

Name: Lua
Description: An Extensible Extension Language
Version: ${version}
Requires:
Libs: -L\${libdir} -llua -lm -ldl
Cflags: -I\${includedir}
EOF

    unset -v short_version

    # Compress manual page(s)
    lzip -9 "${destdir}/${mandir}"/man1/*

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

