# Build recipe for tcp_wrappers.
#
# Copyright (c) 2018, 2021 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=tcp_wrappers
version=7.6
release=3

# Define a category for the output of the package name
pkgcategory=networking

tarname=${program}_${version}.tar.gz

# Remote source(s)
fetch=http://ftp.porcupine.org/pub/security/$tarname

description="
Monitor of incoming TCP connections.

TCP Wrapper is a host-based Networking ACL system, used to
filter network access to Internet Protocol servers on
(Unix-like) operating systems such as Linux or BSD.

It allows host or subnetwork IP addresses, names and/or
ident query replies, to be used as tokens on which to
filter for access control purposes.
"

homepage=ftp://ftp.porcupine.org/pub/security/index.html
license=BSD

# Source documentation
docs="BLURB CHANGES DISCLAIMER README README.NIS"
docsdir="${docdir}/${program}-${version}"

# Source directory
srcdir=${program}_${version}

# Limit parallel jobs for the compiler to 1
jobs=1

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Refresh old source code plus security issues via patches

    # Apply patches from Debian (Thanks!)
    while read -r line
    do
        patch -p1 < "${worktree}/patches/tcp_wrappers/${line}"
    done < "${worktree}/patches/tcp_wrappers/series"

    # Apply extras patches from "musl development overlay" (Thanks to "Gentoo Linux")
    patch -p1 < "${worktree}/patches/tcp_wrappers/tcp-wrappers-7.6.22-remove-DECLS.patch"
    patch -p1 < "${worktree}/patches/tcp_wrappers/tcp-wrappers-7.6-redhat-bug11881.patch"
    patch -p1 < "${worktree}/patches/tcp_wrappers/0001-Remove-fgets-extern-declaration.patch"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    make -j${jobs} COPTS="$QICFLAGS" LDOPTS="$QILDFLAGS" \
     STYLE="-DPROCESS_OPTIONS" REAL_DAEMON_DIR=/usr/sbin musl

    # Install manually

    mkdir -p "${destdir}/usr/include" \
     "${destdir}/usr/sbin" "${destdir}/usr/lib${libSuffix}"

    for file in safe_finger tcpd tcpdchk tcpdmatch try-from
    do
        cp -p $file "${destdir}/usr/sbin/"
        chmod 755   "${destdir}/usr/sbin/${file}"
    done
    unset -v file

    cp -p libwrap.a "${destdir}/usr/lib${libSuffix}/"
    chmod 644 "${destdir}/usr/lib${libSuffix}/libwrap.a"

    cp -p tcpd.h "${destdir}/usr/include/"
    chmod 644 "${destdir}/usr/include/tcpd.h"

    mkdir -p "${destdir}${mandir}/man3" \
     "${destdir}${mandir}/man5" "${destdir}${mandir}/man8"
    lzip -9c < hosts_access.3   > "${destdir}${mandir}/man3/hosts_access.3.lz"
    lzip -9c < hosts_access.5   > "${destdir}${mandir}/man5/hosts_access.5.lz"
    lzip -9c < hosts_options.5  > "${destdir}${mandir}/man5/hosts_options.5.lz"
    lzip -9c < tcpd.8           > "${destdir}${mandir}/man8/tcpd.8.lz"
    lzip -9c < tcpdchk.8        > "${destdir}${mandir}/man8/tcpdchk.8.lz"
    lzip -9c < tcpdmatch.8      > "${destdir}${mandir}/man8/tcpdmatch.8.lz"

    # Strip remaining binaries and libraries
    find "$destdir" -type f | xargs file | \
     awk '/ELF/ && /executable/ || /shared object/' | \
      cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

    strip --strip-debug "${destdir}/usr/lib${libSuffix}/libwrap.a"

    # Copy local config files
    mkdir -p "${destdir}/etc"
    cp -p "${worktree}/archive/tcp_wrappers/etc/hosts.allow" "${destdir}/etc/"
    cp -p "${worktree}/archive/tcp_wrappers/etc/hosts.deny" "${destdir}/etc/"
    chmod 644 "${destdir}/etc/hosts.allow" "${destdir}/etc/hosts.deny"
    touch "${destdir}/etc/.graft-config"

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

