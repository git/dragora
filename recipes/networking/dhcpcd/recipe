# Build recipe for dhcpcd.
#
# Copyright (c) 2017 MMPG, <mmpg@vp.pl>.
# Copyright (c) 2017-2019, 2021-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=dhcpcd
version=9.4.1
release=2

# Define a category for the output of the package name
pkgcategory=networking

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch=https://roy.marples.name/downloads/dhcpcd/$tarname

description="
dhcpcd is an implementation of the DHCP client specified in RFC2131.

A DHCP client is useful for connecting your computer to a network
which uses DHCP to assign network addresses. dhcpcd strives to be
a fully featured, yet very lightweight DHCP client. 
"

homepage=https://roy.marples.name/projects/dhcpcd
license="2-clause BSD"

# Source documentation
docs="LICENSE README.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --libexecdir=/usr/libexec/dhcpcd \
     --dbdir=/var/lib/dhcpcd \
     --privsepuser=dhcpcd \
     --build="$(gcc -dumpmachine)"

    make -j${jobs}
    make -j${jobs} DESTDIR="$destdir" install

    # Adjust ownerships/permissions to use privilege separation

    chmod 700 "${destdir}/var/lib/dhcpcd"
    chown dhcpcd:dhcpcd "${destdir}/var/lib/dhcpcd"

    # Install dhcpcd perp service(s)

    mkdir -p "${destdir}/etc/perp/dhcpcd"

    cp -p "${worktree}/archive/dhcpcd/rc.log" \
          "${worktree}/archive/dhcpcd/rc.main" \
          "${destdir}/etc/perp/dhcpcd/"

    chmod 755 "${destdir}"/etc/perp/dhcpcd/rc.*

    # Disable default service in favor of connmand
    chmod -t "${destdir}/etc/perp/dhcpcd"

    # Manage (dot) new files via graft(1)

    touch "${destdir}/etc/.graft-config" \
          "${destdir}/etc/perp/dhcpcd/.graft-config"

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

