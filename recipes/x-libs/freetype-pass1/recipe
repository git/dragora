# Build recipe for freetype (pass1).
#
# Copyright (c) 2018, 2022-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=freetype
version=2.13.0
release=1

pkgname=freetype-pass1

# Define a category for the output of the package name
pkgcategory=x-libs

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://download.savannah.gnu.org/releases/freetype/$tarname

description="
Temporary installation of freetype.

This is a temporary installation to satisfy the requirements of Xorg.
Later, this will be replaced with the fully \"freetype\" package.
"

homepage=https://www.freetype.org/
license="GPLv2 | FreeType License (FTL)"

# Source documentation
docsdir="${docdir}/${pkgname}-${version}"

build() {
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --mandir=$mandir \
     --docdir=$docsdir \
     --disable-static \
     --enable-shared \
     --with-bzip2=yes \
     --with-zlib=yes \
     --with-png=yes \
     --with-harfbuzz=no \
     --build="$(gcc -dumpmachine)" \
     ac_cv_prog_RC= ac_cv_prog_ac_ct_RC=

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Strip remaining binaries and libraries
    find "$destdir" -type f | xargs file | \
     awk '/ELF/ && /executable/ || /shared object/' | \
      cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

    # Compress manual pages
    if [ -d "${destdir}/$mandir" ] ; then
	(
            cd "${destdir}/$mandir"
	    find . -type f -exec lzip -9 {} +
	    find . -type l | while read -r file
	    do
		ln -sf "$(readlink -- "$file").lz" "${file}.lz"
		rm -- "$file"
	    done
	)
    fi
}

