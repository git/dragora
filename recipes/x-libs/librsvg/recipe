# Build recipe for librsvg.
#
# Copyright (C) 2018, MMPG <mmpg@vp.pl>
# Copyright (c) 2018-2019, 2021 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=librsvg
version=2.54.5
release=1

# Define a category for the output of the package name
pkgcategory=x-libs

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch="https://download.gnome.org/sources/librsvg/${version%.*}/$tarname"

description="
A high performance SVG rendering library.

The librsvg package contains librsvg libraries and tools used to
manipulate, convert and view Scalable Vector Graphic images.
"

homepage=https://wiki.gnome.org/Projects/LibRsvg
license="GPLv2+, LGPLv2+"

# Source documentation
docs="AUTHORS CONTRIBUTING.md COPYING* NEWS README.md code-of-conduct.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" \
    LDFLAGS="$QILDFLAGS -Wl,--no-keep-memory,-z,defs -Wl,-O1 -Wl,--as-needed" \
    $configure_args \
    --libdir=/usr/lib${libSuffix} \
    --mandir=$mandir \
    --docdir=$docsdir \
    --enable-static=no \
    --enable-shared=yes \
    --enable-pixbuf-loader \
    --enable-introspection \
    --enable-vala \
    --disable-rpath \
    --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} V=1 DESTDIR="$destdir" install-strip

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
    rm "${destdir}/${docsdir}"/COMPILING.md;	# Unnecessary here.
}

