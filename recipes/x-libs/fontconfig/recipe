# Build recipe for fontconfig.
#
# Copyright (c) 2017 Mateus P. Rodrigues, <mprodrigues@dragora.org>.
# Copyright (c) 2017-2018 Matias Fonzo, <selk@dragora.org>.
# Copyright (c) 2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=fontconfig
version=2.14.1
release=1

# Define a category for the output of the package name
pkgcategory=x-libs

# The installation of this package replaces to
replace=fontconfig-pass1

description="
Library for font configuration.

The fontconfig package is a library for configuring and customizing
font access.
"

homepage=https://www.freedesktop.org/wiki/Software/fontconfig/
license=Custom

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://www.freedesktop.org/software/fontconfig/release/$tarname

# Source documentation
docs="AUTHORS COPYING ChangeLog NEWS README"
docsdir="${docdir}/${program}-${version}"

build() {
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Make sure the system regenerates src/fcobjshash.h (Thanks to BLFS!)
    rm -f src/fcobjshash.h

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
    $configure_args \
    --libdir=/usr/lib${libSuffix} \
    --mandir=$mandir \
    --with-docdir=$docsdir \
    --disable-static \
    --enable-shared \
    --disable-gtk-doc \
    --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress manual pages
    if [ -d "${destdir}/$mandir" ] ; then
	(
            cd "${destdir}/$mandir"
	    find . -type f -exec lzip -9 {} +
	    find . -type l | while read -r file
	    do
		ln -sf "$(readlink -- "$file").lz" "${file}.lz"
		rm -- "$file"
	    done
	)
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

