# Build recipe for at-spi2-atk.
#
# Copyright (C) 2018, MMPG <mmpg@vp.pl>
# Copyright (C) 2018-2019, 2021 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=at-spi2-atk
version=2.38.0
release=1

# Define a category for the output of the package name
pkgcategory=x-libs

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch="https://ftp.gnome.org/pub/gnome/sources/at-spi2-atk/${version%.*}/$tarname"

description="
A GTK+ module that bridges ATK to the new D-Bus based AT-SPI.

This package includes libatk-bridge, a library that bridges ATK to the
new D-Bus based AT-SPI, as well as a corresponding module for gtk+ 2.x.
Gtk+ 3.x now links against libatk-bridge directly rather than requiring
it to be loaded as a module.

These libraries depend on the at-spi2-core code that contains the
daemon for registering applications, D-Bus helper libraries and the
AT-SPI D-Bus specifications.
"

homepage=https://wiki.gnome.org/Accessibility/
license=LGPLv2+

docs="AUTHORS COPYING NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    mkdir BUILD
    cd BUILD

    CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
    meson setup $configure_args \
     --libdir /usr/lib${libSuffix} \
     --buildtype=release \
     --strip \
     ..

    ninja -j${jobs}
    DESTDIR="$destdir" ninja -j${jobs} install

    cd ..

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

