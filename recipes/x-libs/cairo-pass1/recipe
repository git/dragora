# Build recipe for cairo (pass1).
#
# Copyright (C) 2018, 2021-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=cairo
version=1.17.6
release=2

pkgname=cairo-pass1

# Define a category for the output of the package name
pkgcategory=x-libs

tarname=${program}-${version}.tar.xz

# Remote source(s)
#fetch=https://www.cairographics.org/releases/$tarname
fetch=https://download.gnome.org/sources/cairo/1.17/$tarname

description="
Temporary installation of cairo.

This is a temporary installation that is replaced with additional
support included in the final cairo package.
"

homepage=https://www.cairographics.org/
license="LGPLv2.1 | MPLv1.1"

# Copy documentation
docsdir="${docdir}/${pkgname}-${version}"

build()
{
    unpack "${tardir}/$tarname"
    
    cd "$srcdir"

    # Adapt this package for binutils-2.39 or later (Thanks to BLFS!)
    sed -e 's/PTR/void */' -i util/cairo-trace/lookup-symbol.c

    # Fix a pkg-config file that may cause errors later (Thanks to BLFS!)
    sed -e "/@prefix@/a exec_prefix=@exec_prefix@" \
        -i util/cairo-script/cairo-script-interpreter.pc.in

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .
    
    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" CXXFLAGS="$QICXXFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --enable-static=no \
     --enable-shared=yes \
     --enable-tee \
     --enable-xlib \
     --enable-xcb \
     --enable-ps \
     --enable-ft \
     --enable-gl \
     --enable-svg \
     --enable-xml \
     --enable-gobject \
     --disable-trace \
     --disable-xcb-shm \
     --disable-xlib-xcb \
     --disable-gtk-doc \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi
}

