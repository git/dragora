# Build recipe for gnome-themes-extra.
#
# Copyright (c) 2019 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=gnome-themes-extra
version=3.28
release=1

# Define a category for the output of the package name
pkgcategory=x-libs

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch="https://ftp.gnome.org/pub/gnome/sources/gnome-themes-extra/${version%.*}/$tarname"

description="
GNOME extra themes.

The GNOME Themes extra package, formerly known as GNOME Themes
Standard, contains various components of the default GNOME theme.
"

homepage=https://www.gnome.org
license=LGPLv2.1

# Source documentation
docs="ChangeLog LICENSE NEWS README.md"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --docdir=$docsdir \
     --disable-nls \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install-strip

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
    chmod 644 "${destdir}/${docsdir}"/*
}

