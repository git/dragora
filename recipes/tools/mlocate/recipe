# Build recipe for mlocate.
#
# Copyright (c) 2017 Alan Beadle, <ab.beadle@gmail.com>.
# Copyright (c) 2017, 2021 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=mlocate
version=0.26
release=2

# Define a category for the output of the package name
pkgcategory=tools

tarname=${program}-${version}.tar.xz

# Remote source(s)
fetch=https://releases.pagure.org/${program}/$tarname

description="
mlocate is a locate/updatedb implementation.

The 'm' stands for 'merging': updatedb reuses the existing database to
avoid rereading most of the file system, which makes updatedb faster
and does not trash the system caches as much.
"

homepage=https://pagure.io/mlocate
license=GPLv2+

# Source documentation
docs="AUTHORS ChangeLog COPYING NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    patch -Np1 -i "${worktree}/patches/mlocate/01-fortify-source-compat.patch"

    # Update for hosts based on musl
    cp -f "${worktree}/archive/common/config.guess" admin/config.guess
    cp -f "${worktree}/archive/common/config.sub" admin/config.sub

    ./configure CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS -static" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --localstatedir=/var/lib \
     --mandir=$mandir \
     --docdir=$docsdir \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} groupname=locate
    make -j${jobs} groupname=locate DESTDIR="$destdir" install

    mv "${destdir}/usr/bin/locate" "${destdir}/usr/bin/mlocate"
    ln -s mlocate "${destdir}/usr/bin/locate"

    # To increase security changing the default 2755
    chmod 2711 "${destdir}/usr/bin/mlocate"

    # Remove generated charset.alias
    rm -f "${destdir}/usr/lib${libSuffix}/charset.alias"
    rmdir "${destdir}/usr/lib${libSuffix}" || true

    # Copy custom files

    mkdir -p "${destdir}/etc/cron.daily/"
    cp -p "${worktree}/archive/mlocate/etc/cron.daily/mlocate" \
          "${destdir}/etc/cron.daily/"
    chmod 0755 "${destdir}/etc/cron.daily/mlocate"
    cp -p "${worktree}/archive/mlocate/etc/updatedb.conf" \
          "${destdir}/etc/"
    chmod 0644 "${destdir}/etc/updatedb.conf"

    # To handle config file(s)
    touch "${destdir}/etc/.graft-config"

    # Compress man pages
    lzip -9 "${destdir}/${mandir}"/man?/*

    # A symbolic link if someone requests locate(1)
    ln -s locate.1.lz "${destdir}/${mandir}/man1/mlocate.1.lz"

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

