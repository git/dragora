# Build recipe for gpm.
#
# Copyright (c) 2018-2020, 2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=gpm
version=20200617_e82d1a6
release=2

# Define a category for the output of the package name
pkgcategory=tools

tarname=${program}-${version}.tar.lz

# Remote source(s)
fetch="
 https://dragora.mirror.garr.it/current/sources/$tarname
 rsync://rsync.dragora.org/current/sources/$tarname
"

description="
Utility and mouse server for virtual consoles.

The gpm daemon tries to be a useful mouse server for applications
running on the GNU/Linux console.  Its roots are in the "selection"
package, by Andrew Haylett, and the original code comes from
selection itself.

This package is intended as a replacement for selection, to provide
additional facilities.
"

homepage=https://www.nico.schottelius.org/software/gpm/
license=GPLv2+

# Source documentation
docs="COPYING README* TODO"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    ./configure CPPFLAGS="$QICPPFLAGS" CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --sysconfdir=/etc/gpm \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install
    strip --strip-debug "${destdir}/usr/lib${libSuffix}/libgpm.a"

    # Emacs already cover this
    rm -rf "${destdir}/usr/share/emacs"

    # Make symlink for compatibility, just in case
    ln -s libgpm.so.2.1.0 "${destdir}/usr/lib${libSuffix}/libgpm.so"

    # Include configuration files for GPM

    mkdir -p "${destdir}/etc/gpm"
    cp -p conf/*.conf "${destdir}/etc/gpm"
    chmod 644 "${destdir}/etc/gpm"/*.conf
    touch "${destdir}/etc/gpm/.graft-config"

    # Include menu-driven tool to configure the mouse

    mkdir -p "${destdir}/usr/sbin"
    cp -p "${worktree}/archive/dragora-tools/dragora-mouse" \
          "${destdir}/usr/sbin"
    chmod 755 "${destdir}/usr/sbin/dragora-mouse"

    # Make compatible symlinks for the invocation.
    #
    # gpmconfig was the name used for Dragora-2
    ln -sf dragora-mouse "${destdir}/usr/sbin/gpmconfig"

    # Perhaps, for Slackware users?
    ln -sf dragora-mouse "${destdir}/usr/sbin/mouseconfig"

    # Compress info documents deleting index file for the package
    if test -d "${destdir}/$infodir"
    then
        rm -f "${destdir}/${infodir}/dir"
        lzip -9 "${destdir}/${infodir}"/*
    fi

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

