# Build recipe for dmenu.
#
# Copyright (c) 2018-2019, 2021-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=dmenu
version=5.2
release=1

# Define a category for the output of the package name
pkgcategory=x-apps

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://dl.suckless.org/tools/$tarname

description="
A dynamic menu for X, related to DWM.

dmenu is a dynamic menu for X, originally designed for dwm.  It manages
large numbers of user-defined menu items efficiently.
"

homepage=https://tools.suckless.org/dmenu/
license=MIT

# Source documentation
docs="LICENSE README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    cat "${worktree}/archive/dmenu/config.mk" > ./config.mk

    make -j${jobs} \
     X11INC=/usr/include/X11 \
     X11LIB=/usr/lib${libSuffix}/X11 \
     FREETYPEINC=/usr/include/freetype2
    make -j${jobs} PREFIX=/usr DESTDIR="$destdir" install

    strip --strip-unneeded "${destdir}/usr/bin"/* 2> /dev/null || true;
    lzip -9 "${destdir}/${mandir}/man1"/*.1

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

