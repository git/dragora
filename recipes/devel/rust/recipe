# Build recipe for rust.
#
# Copyright (c) 2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=rustc
pkgname=rust
version=1.64.0
release=1

# Define a category for the output of the package name
pkgcategory=devel

tarname=${program}-${version}-src.tar.gz

# Set remote source(s) according to the architecture/platform
fetch=https://static.rust-lang.org/dist/$tarname

rustfetch_date=2022-08-11

case $arch in
amd64 | x32 )
    tarname_rust_std_platform=rust-std-1.63.0-x86_64-unknown-linux-musl.tar.xz
    tarname_rustc_platform=rustc-1.63.0-x86_64-unknown-linux-musl.tar.xz
    tarname_cargo_platform=cargo-1.63.0-x86_64-unknown-linux-musl.tar.xz
    fetch="
$fetch
https://static.rust-lang.org/dist/${rustfetch_date}/$tarname_rust_std_platform
https://static.rust-lang.org/dist/${rustfetch_date}/$tarname_rustc_platform
https://static.rust-lang.org/dist/${rustfetch_date}/$tarname_cargo_platform
"
    ;;
i586)
    tarname_rust_std_platform=
    tarname_rustc_platform=
    tarname_cargo_platform=

    fetch="
$fetch
https://static.rust-lang.org/dist/${rustfetch_date}/$tarname_rust_std_platform
https://static.rust-lang.org/dist/${rustfetch_date}/$tarname_rustc_platform
https://static.rust-lang.org/dist/${rustfetch_date}/$tarname_cargo_platform
"
    ;;
esac

description="
The Rust programming language.

The Rust programming language helps you write faster, more reliable software.
High-level ergonomics and low-level control are often at odds in programming
language design; Rust challenges that conflict.  Through balancing powerful
technical capacity and a great developer experience, Rust gives you the
option to control low-level details (such as memory usage) without all the
hassle traditionally associated with such control.
"

homepage=https://www.rust-lang.org/
license="Apachev2.0, MIT"

# Source documentation
docs="";	# This build system already installs the documentation.
docsdir="${docdir}/${pkgname}-${version}"

# The source has a custom directory name
srcdir=${program}-${version}-src

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    mkdir -p build/cache/$rustfetch_date
    cp -f \
     "${tardir}/$tarname_rust_std_platform" \
     "${tardir}/$tarname_rustc_platform" \
     "${tardir}/$tarname_cargo_platform" \
     build/cache/$rustfetch_date

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Apply patches taken from "Void Linux" (Thanks!)
    for file in "${worktree}"/patches/rust/void/*.patch
    do
        if test -f "$file"
        then
            echo "    Applying patch $file ..."
            patch -Np1 -i "$file"
        fi
    done
    unset -v file

    # Uncomment this only if the build of LLVM is static
    #rm -rf src/llvm-project
    #patch -Np1 -i "${worktree}/patches/rust/llvm/llvm-with-dependencies.patch"

    # We need this because cargo verifies checksums of all files in vendor
    # crates when it builds and gives us no way to override or update the
    # file sanely... so just clear out the file list (Thanks to "Void Linux")
    for postdir in libc typenum cc target-lexicon tikv-jemallocator
    do
        sed -i 's/\("files":{\)[^}]*/\1/' vendor/${postdir}/.cargo-checksum.json
    done
    unset -v postdir

    # Insert our custom config.toml
    cp -f "${worktree}/archive/rust/config.toml" config.toml && \
     sed -e "s|@prefix@|/usr|" \
         -e "s|@libdir@|lib${libSuffix}|" \
         -e "s|@docsdir@|$docsdir|" \
         -i config.toml

    # Set flags prior to build

    CPPFLAGS="$QICPPFLAGS"
    CFLAGS="$QICFLAGS"
    CXXFLAGS="$QICXXFLAGS"
    LDFLAGS="$QILDFLAGS"
    SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
    RUST_BACKTRACE=full

    RUSTFLAGS="$RUSTFLAGS -C link-args=-lffi"

    export CPPFLAGS CFLAGS CXXFLAGS LDFLAGS SSL_CERT_FILE RUST_BACKTRACE RUSTFLAGS

    # Build and install Rust

    python3 ./x.py build
    DESTDIR="$destdir" python3 x.py install

    unset -v CPPFLAGS CFLAGS CXXFLAGS LDFLAGS SSL_CERT_FILE RUST_BACKTRACE RUSTFLAGS \
     rustfetch_date tarname_rust_std_platform tarname_rustc_platform tarname_cargo_platform

    # Strip remaining binaries and libraries
    find "$destdir" -type f | xargs file | \
     awk '/ELF/ && /executable/ || /shared object/' | \
      cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi
}

