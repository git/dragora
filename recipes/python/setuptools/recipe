# Build recipe for setuptools.
#
# Copyright (C) 2020-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=setuptools
version=67.6.1
release=1

pkgname=python-${program}

# Define a category for the output of the package name
pkgcategory=python

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://files.pythonhosted.org/packages/b6/21/cb9a8d0b2c8597c83fce8e9c02884bce3d4951e41e807fc35791c6b23d9a/$tarname

description="
Easily build and distribute Python packages.

Setuptools is a fully-featured, actively-maintained, and stable library
designed to facilitate packaging Python projects, where packaging
includes:

- Python package and module definitions
- Distribution package metadata
- Test hooks
- Project installation
- Platform-specific details
- Python 3 support

For more information, see https://setuptools.readthedocs.io/en/latest/
"

homepage=https://pypi.org/project/setuptools/
license=MIT

docs="CHANGES* LICENSE* PKG-INFO README*"
docsdir="${docdir}/${pkgname}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    python3 setup.py build
    python3 setup.py install --prefix=/usr --root="$destdir" --optimize=1

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

