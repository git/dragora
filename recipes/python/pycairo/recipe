# Build recipe for pycairo.
#
# Copyright (C) 2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=pycairo
version=1.23.0
release=1

pkgname=python-${program}

# Define a category for the output of the package name
pkgcategory=python

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://github.com/pygobject/pycairo/releases/download/v${version}/$tarname

description="
Python bindings for Cairo.

The pycairo bindings are designed to match the Cairo C API as closely
as possible, and to deviate only in cases which are clearly better
implemented in a more \"Pythonic way\".
"

homepage=https://pycairo.readthedocs.io/en/latest/
license="LGPLv2.1 | MPLv1.1"

docs="COPYING* NEWS README.rst"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    python3 setup.py build
    python3 setup.py install --prefix=/usr --root="$destdir" --optimize=1

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

