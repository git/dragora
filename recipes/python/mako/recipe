# Build recipe for mako.
#
# Copyright (C) 2019-2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=Mako
version=1.2.4
release=1

pkgname=python-${program}

# Define a category for the output of the package name
pkgcategory=python

tarname=${program}-${version}.tar.gz

# Remote source(s)
fetch=https://files.pythonhosted.org/packages/05/5f/2ba6e026d33a0e6ddc1dddf9958677f76f5f80c236bd65309d280b166d3e/$tarname

description="
Mako is a template library written in Python.

It provides a familiar, non-XML syntax which compiles into Python
modules for maximum performance.  Mako's syntax and API borrows
from the best ideas of many others, including Django and
Jinja2templates, Cheetah, Myghty, and Genshi.  Conceptually,
Mako is an embedded Python (i.e. Python Server Page) language,
which refines the familiar ideas of componentized layout and
inheritance to produce one of the most straightforward and
flexible models available, while also maintaining close ties
to Python calling and scoping semantics.

The package name is called \"pymako\" in order to avoid possible
conflicts with another package name.
"

homepage=https://www.makotemplates.org
license=MIT

docs="AUTHORS CHANGES LICENSE PKG-INFO README.rst"
docsdir="${docdir}/${pkgname}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    python3 setup.py build
    python3 setup.py install --prefix=/usr --root="$destdir" --optimize=1

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

