# Build recipe for libpaper.
#
# Copyright (c) 2022 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Exit immediately on any error
set -e

program=libpaper
version=1.1.28
release=1

# Define a category for the output of the package name
pkgcategory=printing

tarname=${program}_${version}.tar.gz

# Remote source(s)
fetch=http://deb.debian.org/debian/pool/main/libp/libpaper/$tarname

description="
Paper-handling library.

The libpaper paper-handling library automates recognition of many different
paper types and sizes for programs that need to deal with printed output.
"

homepage=https://packages.qa.debian.org/libp/libpaper.html
license=GPLv2+

# Source documentation
docs="AUTHORS COPYING NEWS README"
docsdir="${docdir}/${program}-${version}"

build()
{
    unpack "${tardir}/$tarname"

    cd "$srcdir"

    # Set sane permissions
    chmod -R u+w,go-w,a+rX-s .

    # Regeneration of the build system is needed
    autoreconf -vif

    ./configure CPPFLAGS="$QICPPFLAGS" \
    CFLAGS="$QICFLAGS" LDFLAGS="$QILDFLAGS" \
     $configure_args \
     --libdir=/usr/lib${libSuffix} \
     --infodir=$infodir \
     --mandir=$mandir \
     --docdir=$docsdir \
     --enable-static=no \
     --enable-shared=yes \
     --build="$(gcc -dumpmachine)"

    make -j${jobs} V=1
    make -j${jobs} DESTDIR="$destdir" install

    # Create (expected) directory for packages
    mkdir -p "${destdir}/etc/libpaper.d"

    # Set the default paper size
    cat << EOF > "${destdir}/etc/papersize"
a4
EOF

    # To handle "papersize" file as ".new"
    touch "${destdir}/etc/.graft-config"

    # Compress and link man pages (if needed)
    if test -d "${destdir}/$mandir"
    then
        (
            cd "${destdir}/$mandir"
            find . -type f -exec lzip -9 {} +
            find . -type l | while read -r file
            do
                ln -sf "$(readlink -- "$file").lz" "${file}.lz"
                rm -- "$file"
            done
        )
    fi

    # Copy documentation
    mkdir -p "${destdir}/$docsdir"
    cp -p $docs "${destdir}/$docsdir"
}

