Dragora
=======

Dragora is an independent GNU/Linux distribution project which was created
from scratch with the intention of providing a reliable operating system with
maximum respect for the user by including entirely free software.  Dragora is
based on the concepts of simplicity and elegance, it offers a user-friendly
Unix-like environment with emphasis on stability and security for long-term
durability.

- Website: <https://www.dragora.org>
- Mailing List: <https://lists.nongnu.org/mailman/listinfo/dragora-users>
- Issue Tracker: <https://notabug.org/dragora/dragora/issues>

For real-time feeback, join us on IRC (`#dragora` on `irc.libera.chat`).
