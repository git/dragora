# Build script for gcc.
#
# Copyright (c) 2016-2023 Matias Fonzo, <selk@dragora.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

version=11-20230310

# Prerequisites
gmp_version=6.2.1
mpfr_version=4.2.0
mpc_version=1.3.1
isl_version=0.24

cd -- "$TMPDIR"

rm -rf gcc-${version} gmp-${gmp_version} \
       mpfr-${mpfr_version} mpc-${mpc_version} isl-${isl_version}
unpack "${worktree}/sources/gcc-${version}.tar.xz" \
       "${worktree}/sources/gmp-${gmp_version}.tar.lz" \
       "${worktree}/sources/mpfr-${mpfr_version}.tar.bz2" \
       "${worktree}/sources/mpc-${mpc_version}.tar.gz" \
       "${worktree}/sources/isl-${isl_version}.tar.bz2"

# Build instructions
cd gcc-${version}

# Make symlinks for requisites
ln -s ../gmp-${gmp_version} gmp
ln -s ../mpfr-${mpfr_version} mpfr
ln -s ../mpc-${mpc_version} mpc
ln -s ../isl-${isl_version} isl

# Apply patches for MPFR
(
    cd mpfr || exit 1

    for file in "${worktree}"/patches/mpfr/*
    do
        if test -f "$file"
        then
            rm -f PATCHES
            patch -p1 < "$file"
        fi
    done
)

# Update detection for hosts based on musl

cp -f "${worktree}/archive/common/config.guess" gmp/configfsf.guess
cp -f "${worktree}/archive/common/config.sub" gmp/config.sub

cp -f "${worktree}/archive/common/config.guess" isl/config.guess
cp -f "${worktree}/archive/common/config.sub" isl/config.sub

# Apply specific patches for the support in musl.
# https://port70.net/~nsz/musl/gcc-11.1.0/

patch -Np1 -i "${worktree}/patches/gcc/11/0002-posix_memalign.patch"
patch -Np1 -i "${worktree}/patches/gcc/11/0003-j2.patch"
patch -Np1 -i "${worktree}/patches/gcc/11/0004-static-pie.patch"
patch -Np1 -i "${worktree}/patches/gcc/11/0005-m68k-sqrt.patch"
patch -Np1 -i "${worktree}/patches/gcc/11/extra-musl_libssp.patch"

patch -Np1 -i "${worktree}/patches/gcc/0008-Disable-ssp-on-nostdlib-nodefaultlibs-and-ffreestand.patch"

# Build in a separate directory
rm -rf ../gcc-build
mkdir ../gcc-build
cd ../gcc-build

../gcc-${version}/configure \
AR="ar" CC="$BTCC" CXX="$BTCXX" \
CFLAGS="$BTCFLAGS" CXXFLAGS="$BTCXXFLAGS" LDFLAGS="$BTLDFLAGS" \
 --prefix="$crossdir" \
 --libdir="${crossdir}/lib${libSuffix}" \
 --build=$host \
 --host=$host \
 --target=$target \
 --enable-languages=c \
 --enable-clocale=generic \
 --enable-initfini-array \
 --disable-shared \
 --disable-threads \
 --disable-decimal-float \
 --disable-libgomp \
 --disable-libssp \
 --disable-libatomic \
 --disable-libitm \
 --disable-libquadmath \
 --disable-libvtv \
 --disable-libcilkrts \
 --disable-libstdcxx \
 --disable-gnu-indirect-function \
 --disable-libsanitizer \
 --disable-libmpx \
 --disable-nls \
 --with-sysroot="${crossdir}/$target" \
 --with-newlib \
 --without-headers \
 --without-ppl \
 --without-cloog \
 $multilib_options \
 $gcc_options

make -j${jobs} MAKEINFO="true" all-gcc all-target-libgcc
make -j${jobs} MAKEINFO="true" install-gcc install-target-libgcc

cd ..

cleanup()
{
    cd -- "$TMPDIR" && rm -rf gcc-${version} gcc-build \
     gmp-${gmp_version} mpfr-${mpfr_version} \
     mpc-${mpc_version} isl-${isl_version}
}

