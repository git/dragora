# Build script for squashfs4.3+.
#
# Copyright (c) 2018-2020, 2023 Matias Fonzo, <selk@dragora.org>.
# Copyright (c) 2022 DustDFG, <dfgdust@gmail.com>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

version=20230406_cbaa34a

cd -- "$TMPDIR"
rm -rf squashfs-tools-${version} \
       "${rootdir}/squashfs-tools_lzip"
unpack "${worktree}/sources/squashfs-tools-${version}.tar.lz"

# Build instructions
cd squashfs-tools-${version}

# Note:
# The build environment for cross-compilation is not imported,
# the squashfs-tools installation must work from the host system,
# this will produce the compressed squash-image

# Apply a patch to include LZIP support
patch -p1 < "${worktree}/patches/squashfs-tools/squashfs-tools-20221209_c901403_lzip-0.diff"

cd squashfs-tools

##
# We turn OFF the LZIP support for now until further research in
# order to have a correct behavior while booting the live CD.
##

make -j${jobs} CC=cc LDFLAGS="" \
 GZIP_SUPPORT=1 \
 LZIP_SUPPORT=0 \
 XZ_SUPPORT=0 \
 LZO_SUPPORT=0 \
 LZ4_SUPPORT=0 \
 ZSTD_SUPPORT=0 \
 COMP_DEFAULT=gzip \
 XATTR_SUPPORT=1 \
 install INSTALL_DIR="${rootdir}/squashfs-tools_lzip"

cleanup()
{
    cd -- "$TMPDIR" && rm -rf squashfs-tools-${version}
}

